motorica-org/telemetry.matrix_android_toy
=========================================

A temporary project for experimenting with Matrix.org on Android in Telemetry context (i.e. bg message sending on external triggers, such as BLE notifications).

Shall soon be merged into https://gitlab.com/motorica-org/telemetry-ga-android.
