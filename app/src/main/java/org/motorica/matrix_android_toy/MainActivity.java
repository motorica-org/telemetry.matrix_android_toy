package org.motorica.matrix_android_toy;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;
import org.matrix.androidsdk.HomeserverConnectionConfig;
import org.matrix.androidsdk.MXDataHandler;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.data.MXMemoryStore;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.client.LoginRestClient;
import org.matrix.androidsdk.rest.client.RoomsRestClient;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.RoomResponse;
import org.matrix.androidsdk.rest.model.login.Credentials;

public class MainActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            Credentials credentials = Credentials.fromJson(new JSONObject(getString(R.string.matrix_token)));

            HomeserverConnectionConfig hsConfig = new HomeserverConnectionConfig(Uri.parse(getString(R.string.matrix_server)), credentials);
            if (credentials != hsConfig.getCredentials()) { throw new AssertionError(); }
            Log.i(TAG, "onStart: " + hsConfig);

            MXDataHandler mxDataHandler = new MXDataHandler(new MXMemoryStore(), credentials, new MXDataHandler.InvalidTokenListener() {
                @Override
                public void onTokenCorrupted() {
                    Log.e(TAG, "onTokenCorrupted: ");
                }
            });
            if (hsConfig.getCredentials() != mxDataHandler.getCredentials()) { throw new AssertionError(); }

            MXSession mxSession = new MXSession(hsConfig, mxDataHandler, this);
            if (mxDataHandler.getCredentials() != mxSession.getCredentials()) { throw new AssertionError(); }

            //Log.i(TAG, "onStart: user: " + mxSession.getMyUserId());
            Log.i(TAG, "onStart: isOnline? " + mxSession.isOnline());

            RoomsRestClient roomsApiClient = mxSession.getRoomsApiClient();
            Log.i(TAG, "onStart: " + roomsApiClient.getCredentials());
            if (mxSession.getCredentials() != roomsApiClient.getCredentials()) { throw new AssertionError(); }

            roomsApiClient.initialSync(getString(R.string.matrix_room), new ApiCallback<RoomResponse>() {
                @Override
                public void onSuccess(RoomResponse roomResponse) {

                }

                @Override
                public void onNetworkError(Exception e) {

                }

                @Override
                public void onMatrixError(MatrixError matrixError) {
                    Log.e(TAG, "onMatrixError: " + matrixError.mErrorBodyAsString);
                }

                @Override
                public void onUnexpectedError(Exception e) {

                }
            });

            JsonObject message = (new JsonParser()).parse("{\"msgtype\": \"m.text\", \"body\": \"HELO world!\"}").getAsJsonObject();
            roomsApiClient.sendEventToRoom(getString(R.string.matrix_room), "m.room.message", message, new ApiCallback<Event>() {
                @Override
                public void onSuccess(Event event) {
                    Log.i(TAG, "onSuccess: " + event);
                }

                @Override
                public void onNetworkError(Exception e) {
                    Log.e(TAG, "onNetworkError: ", e);
                }

                @Override
                public void onMatrixError(MatrixError matrixError) {
                    Log.e(TAG, "onMatrixError: " + matrixError.mErrorBodyAsString);
                }

                @Override
                public void onUnexpectedError(Exception e) {
                    Log.e(TAG, "onUnexpectedError: ", e);
                }
            });

            JsonObject message2 = (new JsonParser()).parse("{\"msgtype\": \"m.text\", \"body\": \"EHLO world!\"}").getAsJsonObject();
            roomsApiClient.sendEventToRoom(getString(R.string.matrix_room), "m.room.message", message2, new ApiCallback<Event>() {
                @Override
                public void onSuccess(Event event) {
                    Log.i(TAG, "onSuccess: " + event);
                }

                @Override
                public void onNetworkError(Exception e) {
                    Log.e(TAG, "onNetworkError: ", e);
                }

                @Override
                public void onMatrixError(MatrixError matrixError) {
                    Log.e(TAG, "onMatrixError: " + matrixError.mErrorBodyAsString);
                }

                @Override
                public void onUnexpectedError(Exception e) {
                    Log.e(TAG, "onUnexpectedError: ", e);
                }
            });
        }
        catch (JSONException e) {

        }
        }
}
